import '../../api/Documents/methods';
import '../../api/Documents/server/publications';

import '../../api/OAuth/server/methods';

import '../../api/Users/server/methods';
import '../../api/Users/server/publications';

import '../../api/Utility/server/methods';

import '../../api/Chat/methods';

import '../../api/Responses/methods';
import '../../api/Responses/server/publications';

import '../../api/Drinks/methods';
import '../../api/Drinks/server/publications';