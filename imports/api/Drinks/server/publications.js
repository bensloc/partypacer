import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Drinks from '../Drinks';

Meteor.publish('drinks', function drinks() {
  return Drinks.find({ owner: this.userId });
});

Meteor.publish('drinks.all', function drinks() {
    return Drinks.find({'data.Brand': {$exists:true}});
  });
  

// Note: drinks.view is also used when editing an existing drink.
Meteor.publish('drinks.view', function drinksView(drinkId) {
  check(drinkId, String);
  return Drinks.find({ _id: drinkId, owner: this.userId });
});
