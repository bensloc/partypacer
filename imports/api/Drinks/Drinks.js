import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Drinks = new Mongo.Collection('Drinks');

Drinks.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Drinks.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Drinks.schema = new SimpleSchema({
  owner: {
    type: String,
    label: 'The ID of the user this document belongs to.',
  },
  createdAt: {
    type: String,
    label: 'The date this document was created.',
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    },
  },
  updatedAt: {
    type: String,
    label: 'The date this document was last updated.',
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    },
  },
  data: {
    type: Object,
    label: 'The body of the document.',
    blackbox: true,
    optional: true,
  },
  image: {
    type: String,
    blackbox:true,
    optional: true,
  }
});

Drinks.attachSchema(Drinks.schema);

export default Drinks;
