import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Responses from '../Responses';

Meteor.publish('responses', function responses() {
  return Responses.find({ owner: this.userId },{limit:10, sort:{createdAt:-1}});
});

// Note: documents.view is also used when editing an existing document.
Meteor.publish('responses.view', function responsesView(responseId) {
  check(responseId, String);
  return Responses.find({ _id: responseId, owner: this.userId });
});
