import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Responses from './Responses';
import rateLimit from '../../modules/rate-limit';

Meteor.methods({
  'responses.insert': function responsesInsert(doc) {
    check(doc, {
      reply: String,
      body: String,
    });

    try {
      return Responses.insert({ 
        owner: this.userId,
        from: this.userId,
        type: 'me',
         ...doc 
        });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'responses.update': function responsesUpdate(doc) {
    check(doc, {
      _id: String,
      title: String,
      body: String,
    });

    try {
      const documentId = doc._id;
      Responses.update(documentId, { $set: doc });
      return documentId; // Return _id so we can redirect to document after update.
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'responses.remove': function responsesRemove(documentId) {
    check(documentId, String);

    try {
      return Responses.remove(documentId);
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
});

rateLimit({
  methods: [
    'responses.insert',
    'responses.update',
    'responses.remove',
  ],
  limit: 5,
  timeRange: 1000,
});
