/* eslint-disable consistent-return */

import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Responses = new Mongo.Collection('Responses');

Responses.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Responses.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Responses.schema = new SimpleSchema({
  owner: {
    type: String,
    label: 'The ID of the user this response belongs to.',
  },
  createdAt: {
    type: String,
    label: 'The date this response was created.',
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    },
  },
  updatedAt: {
    type: String,
    label: 'The date this response was last updated.',
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    },
  },
  text: {
    type: String,
    label: 'The title of the response.',
  },
  type:{
    type: String,
    label: 'The type of response',
  },
  buttons: {
    type: Array,
    optional: true,
    label: 'action buttons',
    blackbox: true
  },
  'buttons.$' :{
    type: Object,
    blackbox: true,
    optional: true
  },
  latitude: {
    type: String,
    optional: true
  },
  longitude:{
    type: String,
    optional: true,
  },
  from:{
    type: String,
    label: 'Who the message is from',
  },
  extra:{
    type: Object,
    blackbox: true,
    optional: true,
  },
  image:{
    type: String,
    optional:true,
  }
});

Responses.attachSchema(Responses.schema);

export default Responses;
