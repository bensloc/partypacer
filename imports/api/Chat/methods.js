import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { HTTP } from 'meteor/http';
import Responses from '../Responses/Responses';
import Drinks from '../Drinks/Drinks';

Meteor.methods({
    'chat': function chat(question) {
      check(question, Object);
      try {
        var options = {
          data: {
            text: question.text,
            image: question.image,
            longitude: question.location.longitude,
            latitude: question.location.latitude,
          },
        }
        console.log(options)
        let url = 'http://192.168.90.81:9000/chat';
        Responses.insert({
          owner: this.userId,
          from: this.userId,
          type: 'me',
          longitude: question.location.longitude,
          latitude: question.location.latitude,
          text: question.text,
          image: question.image
        });
        let response = HTTP.call('POST', url ,options);
        console.log(response);
        if(question.image){
          if (response.data.reply){
          Drinks.insert({
            owner: this.userId,
            data: response.data.reply,
            image: question.image,
          });
          Responses.insert({
            owner: this.userId,
            from: 'bot',
            type: response.data.type,
            text: 'Thanks, sweet pick', 
            buttons: response.data.buttons
          });
          }else{
            console.log('no drink');
            Responses.insert({
              owner: this.userId,
              from: 'bot',
              type: response.data.type,
              text: 'Sadly we could not find any bevvies!', 
              buttons: response.data.buttons
            });
          }
        }else{
          Responses.insert({
            owner: this.userId,
            from: 'bot',
            type: response.data.type,
            text: response.data.reply, 
            buttons: response.data.buttons
          });
        }
        return response.data;
      } catch (error) {
        throw new Meteor.Error('500', error);
      }
    },
    'chat.button': function chat(data,text,location){
      check(data, Object);
      check(text, String);
      check(location, Object);
      try {
        data.latitude = location.latitude;
        data.longitude = location.longitude;
        var options = {
          data: data,
        }
        console.log(options);
        var url = 'http://192.168.90.81:9000/chat/command';
        Responses.insert({
          owner: this.userId,
          from: this.userId,
          type: 'me',
          text: text, 
        });
        //calls api chatbot
        var response = HTTP.call('POST', url ,options);
        //adds to collection
        console.log(response);
        //Check response, if yes, add beer, if no take photo.
          if (response.data.type === 'add_beer'){
              Drinks.insert({
                  owner: this.userId,
                  data: response.data.beer,
              });
          }
        Responses.insert({
          owner: this.userId,
          from: 'bot',
          type: response.data.type,
          text: response.data.reply, 
          buttons: response.data.buttons,
          extra: response.data.bar,
        });
        return response.data;
      } catch (error) {
        throw new Meteor.Error('500', error);
      }
    }
  });
  