
import React from 'react';
import { Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import OAuthLoginButtons from '../../components/OAuthLoginButtons/OAuthLoginButtons';
import AccountPageFooter from '../../components/AccountPageFooter/AccountPageFooter';
import validate from '../../../modules/validate';
import {TextField, FlatButton} from 'material-ui';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const component = this;

    validate(component.form, {
      rules: {
        emailAddress: {
          required: true,
          email: true,
        },
        password: {
          required: true,
        },
      },
      messages: {
        emailAddress: {
          required: 'Need an email address here.',
          email: 'Is this email address correct?',
        },
        password: {
          required: 'Need a password here.',
        },
      },
      submitHandler() { component.handleSubmit(); },
    });
  }

  handleSubmit() {
    const { history } = this.props;

    Meteor.loginWithPassword(this.emailAddress.getValue(), this.password.getValue(), (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Welcome back!', 'success');
      }
    });
  }

  render() {
    return (
      <div>
        <OAuthLoginButtons
                services={['facebook', 'github', 'google']}
                emailMessage={{
                  offset: 100,
                  text: 'Log In with an Email Address',
                }}
              />
          <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
              <TextField
              hintText="Email address"
              type='email'
              name='emailAddress'
              ref={emailAddress => (this.emailAddress = emailAddress)}
              fullWidth={true}
              />
              <br/>
              <TextField
              hintText="Password"
              type='password'
              name='password'
              ref={password => (this.password = password)}
              fullWidth={true}
              />
              <br/>
              <FlatButton fullWidth={true} label='Log In' type='submit'/>
            <AccountPageFooter>
              <p>{'Don\'t have an account?'} <Link to="/signup">Sign Up</Link>.</p>
              <p>Forgot your password? <Link to="/recover-password">Reset Password</Link>.</p>
            </AccountPageFooter>
          </form>
          </div>
       );
  }
}

Login.propTypes = {
  history: PropTypes.object.isRequired,
};

export default Login;
