import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import DocumentsCollection from '../../../api/Documents/Documents';
import Loading from '../../components/Loading/Loading';
import {ListItem, List, FlatButton,IconButton, FontIcon} from 'material-ui';

import './Documents.scss';

const handleRemove = (documentId) => {
  if (confirm('Are you sure? This is permanent!')) {
    Meteor.call('documents.remove', documentId, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Document deleted!', 'success');
      }
    });
  }
};

const Documents = ({ loading, documents, match, history }) => (!loading ? (
  <div className="Documents">
      <List>
        <ListItem 
            primaryText='Documents'
            rightIcon={<FontIcon className="material-icons" onClick={()=>history.push(`${match.url}/new`)}>add</FontIcon>}
            />
      </List>
    {documents.length ? <List>
        {documents.map(({ _id, title, createdAt, updatedAt }) => (
          <ListItem key={_id}
            primaryText={title}
            secondaryText={timeago(updatedAt)+ ' ' + monthDayYearAtTime(createdAt)}
            onClick={() => history.push(`${match.url}/${_id}`)}
            rightIcon={<FontIcon className="material-icons" onClick={()=>handleRemove(_id)}>delete</FontIcon>}
          />
        ))}
    </List> : <List><ListItem primaryText="Nothing to see here yet." /></List>}
  </div>
) : <Loading />);

Documents.propTypes = {
  loading: PropTypes.bool.isRequired,
  documents: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('documents');
  return {
    loading: !subscription.ready(),
    documents: DocumentsCollection.find().fetch(),
  };
}, Documents);
