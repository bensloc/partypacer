/* eslint-disable no-underscore-dangle */

import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import _ from 'lodash';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Bert } from 'meteor/themeteorchef:bert';
import { createContainer } from 'meteor/react-meteor-data';
import InputHint from '../../components/InputHint/InputHint';
import validate from '../../../modules/validate';
import {TextField, FlatButton} from 'material-ui';

import './Profile.scss';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newpassword: '',
      currentpassword: '',
    }
    this.getUserType = this.getUserType.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderOAuthUser = this.renderOAuthUser.bind(this);
    this.renderPasswordUser = this.renderPasswordUser.bind(this);
    this.renderProfileForm = this.renderProfileForm.bind(this);
  }

  handleChangeNewPassword = (event) => this.setState({newpassword: event.target.value});
  handleChangeCurrentPassword = (event) => this.setState({currentpassword: event.target.value});

  componentDidMount() {
    const component = this;

    validate(component.form, {
      rules: {
        firstName: {
          required: true,
        },
        lastName: {
          required: true,
        },
        emailAddress: {
          required: true,
          email: true,
        },
        currentPassword: {
          required() {
            // Only required if newPassword field has a value.
            return component.newPassword.getValue().length > 0;
          },
        },
        newPassword: {
          required() {
            // Only required if currentPassword field has a value.
            return component.currentPassword.getValue().length > 0;
          },
        },
      },
      messages: {
        firstName: {
          required: 'What\'s your first name?',
        },
        lastName: {
          required: 'What\'s your last name?',
        },
        emailAddress: {
          required: 'Need an email address here.',
          email: 'Is this email address correct?',
        },
        currentPassword: {
          required: 'Need your current password if changing.',
        },
        newPassword: {
          required: 'Need your new password if changing.',
        },
      },
      submitHandler() { component.handleSubmit(); },
    });
  }

  getUserType(user) {
    const userToCheck = user;
    delete userToCheck.services.resume;
    const service = Object.keys(userToCheck.services)[0];
    return service === 'password' ? 'password' : 'oauth';
  }

  handleSubmit() {
    const profile = {
      emailAddress: this.emailAddress.getValue(),
      profile: {
        name: {
          first: this.firstName.getValue(),
          last: this.lastName.getValue(),
        },
      },
    };

    Meteor.call('users.editProfile', profile, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Profile updated!', 'success');
      }
    });

    if (this.newPassword.getValue()) {
      Accounts.changePassword(this.currentPassword.getValue(), this.newPassword.getValue(), (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          this.state.newpassword = '';
          this.state.currentpassword = '';
        }
      });
    }
  }

  renderOAuthUser(loading, user) {
    return !loading ? (<div className="OAuthProfile">
      {Object.keys(user.services).map(service => (
        <div key={service} className={`LoggedInWith ${service}`}>
          <img src={`/${service}.svg`} alt={service} />
          <p>{`You're logged in with ${_.capitalize(service)} using the email address ${user.services[service].email}.`}</p>
          <Button
            className={`btn btn-${service}`}
            href={{
              facebook: 'https://www.facebook.com/settings',
              google: 'https://myaccount.google.com/privacy#personalinfo',
              github: 'https://github.com/settings/profile',
            }[service]}
            target="_blank"
          >Edit Profile on {_.capitalize(service)}</Button>
        </div>
      ))}
    </div>) : <div />;
  }

  renderPasswordUser(loading, user) {
    return !loading ? (<div>
      <TextField
      type="text"
      name="firstName"
      defaultValue={user.profile.name.first}
      ref={firstName => (this.firstName = firstName)}
      floatingLabelText="First Name"
      fullWidth={true}
    />
    <br/>
    <TextField
      type="text"
      name="lastName"
      defaultValue={user.profile.name.last}
      ref={lastName => (this.lastName = lastName)}
      floatingLabelText="Last Name"
      fullWidth={true}
    />
    <br/>
  <TextField
    type="email"
    name="emailAddress"
    defaultValue={user.emails[0].address}
    ref={emailAddress => (this.emailAddress = emailAddress)}
    floatingLabelText='Email Address'
    fullWidth={true}
  />
  <br/>
  <TextField
    type="password"
    name="currentPassword"
    hintText='Current Password'
    value={this.state.currentpassword}
    onChange={this.handleChangeCurrentPassword}
    ref={currentPassword => (this.currentPassword = currentPassword)}
    fullWidth={true}
  />
  <br/>
  <TextField
    type="password"
    name="newPassword"
    floatingLabelText="New Password"
    hintText='Use at least 6 characters'
    value={this.state.newpassword}
    onChange={this.handleChangeNewPassword}
    ref={newPassword => (this.newPassword = newPassword)}
    fullWidth={true}
  />
  <br/>
<FlatButton type="submit" fullWidth={true}>Save Profile</FlatButton>
</div>) : <div />;
  }

  renderProfileForm(loading, user) {
    return !loading ? ({
      password: this.renderPasswordUser,
      oauth: this.renderOAuthUser,
    }[this.getUserType(user)])(loading, user) : <div />;
  }

  render() {
    const { loading, user } = this.props;
    return (<div className="Profile">
      <h4 className="page-header">Edit Profile</h4>
      <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
        {this.renderProfileForm(loading, user)}
      </form>
    </div>);
  }
}

Profile.propTypes = {
  loading: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('users.editProfile');

  return {
    loading: !subscription.ready(),
    user: Meteor.user(),
  };
}, Profile);
