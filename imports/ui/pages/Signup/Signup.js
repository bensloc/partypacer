import React from 'react';
import { Row, Col, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Bert } from 'meteor/themeteorchef:bert';
import OAuthLoginButtons from '../../components/OAuthLoginButtons/OAuthLoginButtons';
import InputHint from '../../components/InputHint/InputHint';
import AccountPageFooter from '../../components/AccountPageFooter/AccountPageFooter';
import validate from '../../../modules/validate';
import {TextField, FlatButton} from 'material-ui';

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const component = this;

    validate(component.form, {
      rules: {
        firstName: {
          required: true,
        },
        lastName: {
          required: true,
        },
        emailAddress: {
          required: true,
          email: true,
        },
        password: {
          required: true,
          minlength: 6,
        },
      },
      messages: {
        firstName: {
          required: 'What\'s your first name?',
        },
        lastName: {
          required: 'What\'s your last name?',
        },
        emailAddress: {
          required: 'Need an email address here.',
          email: 'Is this email address correct?',
        },
        password: {
          required: 'Need a password here.',
          minlength: 'Please use at least six characters.',
        },
      },
      submitHandler() { component.handleSubmit(); },
    });
  }

  handleSubmit() {
    const { history } = this.props;

    Accounts.createUser({
      email: this.emailAddress.getValue(),
      password: this.password.getValue(),
      profile: {
        name: {
          first: this.firstName.getValue(),
          last: this.lastName.getValue(),
        },
      },
    }, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Meteor.call('users.sendVerificationEmail');
        Bert.alert('Welcome!', 'success');
        history.push('/chat');
      }
    });
  }

  render() {
    return (<div className="Signup">
              <OAuthLoginButtons
                services={['facebook', 'github', 'google']}
                emailMessage={{
                  offset: 97,
                  text: 'Sign Up with an Email Address',
                }}
              />
          <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
          <TextField
          hintText="First Name"
          type='text'
          name='firstName'
          ref={firstName => (this.firstName = firstName)}
          fullWidth={true}
          />
          <br/>
          <TextField
          hintText="Last Name"
          type='text'
          name='lastName'
          ref={lastName => (this.lastName = lastName)}
          fullWidth={true}
          />
          <br/>
          <TextField
          hintText="Email address"
          type='email'
          name='emailAddress'
          ref={emailAddress => (this.emailAddress = emailAddress)}
          fullWidth={true}
          />
          <br/>
          <TextField
          floatingLabelText="Password"
          type='password'
          name='password'
          ref={password => (this.password = password)}
          fullWidth={true}
          hintText="Use at least 6 characters"
          />
          <br/>
          <FlatButton fullWidth={true} label='Sign Up' type='submit'/>
          <AccountPageFooter>
            <p>Already have an account? <Link to="/login">Log In</Link>.</p>
          </AccountPageFooter>
        </form>
    </div>);
  }
}

Signup.propTypes = {
  history: PropTypes.object.isRequired,
};

export default Signup;
