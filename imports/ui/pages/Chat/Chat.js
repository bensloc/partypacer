import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import Loading from '../../components/Loading/Loading';
import {ListItem, List, FlatButton,IconButton, FontIcon, GridList, GridTile, Avatar, Divider, Paper} from 'material-ui';
import ChatWidget from '../../components/ChatWidget/ChatWidget';
import ResponsesCollection from '../../../api/Responses/Responses';
import DrinksCollection from '../../../api/Drinks/Drinks';
import DrinkList from '../../components/DrinkList/DrinkList';
let moment = require('moment');


import './Chat.scss';

const style = {height: '120px'};

const imgHeight = {
  height: '100%',
  width: null
};

class Chat extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { loading, documents, match, history, user, responses, users } = this.props;
    return (!loading ? (
    <div className="Chat">
    <div>
      {users.map(({_id})=>(
        <DrinkList key={_id} userId={_id} />
      ))}
    </div>
    </div>
  ) : <Loading />);
  }
}

Chat.propTypes = {
  loading: PropTypes.bool.isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  responses: PropTypes.arrayOf(PropTypes.object).isRequired,
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('users.all');
  const subscription2 = Meteor.subscribe('responses');

  return {
    loading: (!subscription.ready() && !subscription2.ready() ),
    user: Meteor.user(),
    responses: ResponsesCollection.find({},{sort:{createdAt:1}}).fetch(),
    users: Meteor.users.find({}).fetch(),
  };
}, Chat);
