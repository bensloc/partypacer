import React from 'react';
// import "jquery.cookie";
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPosition: {
                latitude: null,
                longitude: null
            },
        };
    }
    
    parsePosition(position) {
        this.setState({
          currentPosition: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
            }
          });
      }
      
    componentDidMount() {
        let video = this.refs.video;
        window.URL = window.URL || window.webkitURL ;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;

        if(navigator.getUserMedia)
        {
            navigator.getUserMedia({video: true},function(stream) {
                video.src = window.URL.createObjectURL(stream);
                console.log(video.src)
            }, function (error) {
                console.error(error)
            });
        }

        const component = this;
        navigator.geolocation.getCurrentPosition((position) => {
          component.parsePosition(position)
        });
        navigator.geolocation.watchPosition((position) => {
              component.parsePosition(position)
          }, (error) => {
              console.log(error)
          }, {
              enableHighAccuracy: true,
              timeout: 10,
              maximumAge: 0
          }
        );
    }

    capture() {
        let video = this.refs.video
        let canvas = this.refs.canvas;
        canvas.width = video.videoWidth || video.width;
        canvas.height = video.videoHeight || video.height;
        let ctx = canvas.getContext('2d', canvas.width, canvas.height);
        ctx.drawImage(video,0,0);
        let dataURL = canvas.toDataURL()
        let image = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        // $.cookie("image", image)
        const question = {
            image: image,
            text: 'Image',
            location: this.state.currentPosition
        };
        Meteor.call('chat', question, (error, response) => {
            if (error) {
              Bert.alert(error.reason, 'something went wrong');
            } else {
              Bert.alert('Nice Pick','success');
              this.props.history.push('/chat');
            }
          });
        
    }


    render() {
        return (
            <div style={{
                    position: "absolute",
                    width: "100%",
                    height: "100%",
                    zIndex: 1,
                    backgroundColor: "black",
                     top: 0,
                    left: 0
                }}>
                <video ref="video" autoPlay style={{
                    position: "absolute",
                    width: "100%",
                    height: "90%",
                    zIndex: 1,
                    top: 0,
                    left: 0

                }}
                />
                <canvas ref="canvas" style={{visibility: "hidden"}}/>
                <button onClick={()=>this.capture()} style={{
                    position: "absolute",
                    width: "20%",
                    height: "10%",
                    backgroundColor: "white",
                    fontSize: "20px",
                    bottom: 0,
                    left: "50%",
                    transform: "translateX(-50%)"
                }}>
                    Capture
                </button>
            </div>
        )
    }
}