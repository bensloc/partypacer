import React from 'react';
import { year } from '@cleverbeagle/dates';
import { Link, withRouter } from 'react-router-dom';
import {Paper, BottomNavigation, FontIcon, BottomNavigationItem, List, ListItem} from 'material-ui';
import ChatWidget from '../../components/ChatWidget/ChatWidget';
import ResponsesCollection from '../../../api/Responses/Responses';

import './Footer.scss';

const copyrightYear = () => {
  const currentYear = year();
  return currentYear === '2017' ? '2017' : `2017-${currentYear}`;
};

const style = {
    order: '3',
    flexShrink: '0',
    flexBasis: '50px',
};

const indexList = {
  '/chat': 1,
}

const _selectedIndex = (location) => {
  let index = null;
  index = indexList[location.pathname];
  return index;
}
let responses = ResponsesCollection.find({},{sort:{createdAt:1}}).fetch()

const Footer = (props) => (
  <Paper style={style} zDepth={1}>
  <Paper style={{maxHeight:'200px', overflowY:'scroll', margin:'0'}} zDepth={0} >
  <List>
    {responses.map(({ _id, text, createdAt, type}) => ( type == 'me' ?
    <div key={_id + 'd1'}>
    <ListItem
      key={_id}
      primaryText={text}
      secondaryText={moment(createdAt).fromNow()}
      leftAvatar={<Avatar>{user.profile.name.first[0]}</Avatar>}
    />
    <Divider key={_id+'d'} inset={true} />
    </div>
    :
    <div key={_id + 'd1'}>
    <ListItem
      key={_id}
      primaryText={text}
      secondaryText={moment(createdAt).fromNow()}
      rightAvatar={<Avatar src='/bestie.jpg'></Avatar>}
    />
    <Divider key={_id+'d'} inset={true} />
    </div>
    ))}
  </List>
  </Paper>
  <ChatWidget history={history} />
  </Paper>
);

Footer.propTypes = {};

export default withRouter(Footer);
