import React from 'react';
import PropTypes from 'prop-types';
import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import PublicNavigation from '../PublicNavigation/PublicNavigation';
import AuthenticatedNavigation from '../AuthenticatedNavigation/AuthenticatedNavigation';
import {AppBar, FlatButton, ToolbarGroup, Drawer, MenuItem } from 'material-ui';

import './Navigation.scss';

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
  }

  handleToggle = () => this.setState({open: !this.state.open});
  
  handleClose = () => this.setState({open: false});

  render() {
    return (
  <AppBar
    style={{minHeight: '64px'}}
    title='Party Besty'
    iconElementRight={!this.props.authenticated ? <PublicNavigation /> : <AuthenticatedNavigation {... this.props}/>}
    onTitleTouchTap={() => this.props.history.push('/chat')}
    onLeftIconButtonTouchTap={() => this.handleToggle()}
    >
    <Drawer
      docked={false}
      width={200}
      open={this.state.open}
      onRequestChange={(open) => this.setState({open})}
      >
      <MenuItem onClick={this.handleClose}>Menu Item</MenuItem>
      <MenuItem onClick={this.handleClose}>Menu Item 2</MenuItem>
    </Drawer>
  </AppBar>
  );}
}

Navigation.propTypes = {
  authenticated: PropTypes.bool.isRequired,
};

export default withRouter(Navigation);
