import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import Loading from '../../components/Loading/Loading';
import {ListItem, List, FlatButton,IconButton, FontIcon, GridList, GridTile, Avatar, Divider, Paper} from 'material-ui';
import ChatWidget from '../../components/ChatWidget/ChatWidget';
import ResponsesCollection from '../../../api/Responses/Responses';
let moment = require('moment');

const style = {
  height: '500px',
}

const styleFooter = {
    order: '3',
    flexShrink: '0',
    flexBasis: '50px',
    padding: '0em 1em',
};

const handleButtonPress = (data,text,location) => {
  Meteor.call('chat.button', data,text,location, (error, response) => {
    if (error) {
      Bert.alert(error.reason, 'something went wrong');
    } else {
      Bert.alert(response.reply,'success');
    }
  });
}


class AuthFooter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        currentPosition: {
            latitude: null,
            longitude: null
        },
    };
}

parsePosition(position) {
  this.setState({
    currentPosition: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
      }
    });
}

  componentDidMount() {
    const component = this;
    navigator.geolocation.getCurrentPosition((position) => {
      component.parsePosition(position)
    });
    navigator.geolocation.watchPosition((position) => {
          component.parsePosition(position)
      }, (error) => {
          console.log(error)
      }, {
          enableHighAccuracy: true,
          timeout: 10,
          maximumAge: 0
      }
    );
  }
  
  componentDidUpdate(){
    if($('#chatBox')[0]){
      $('#chatBox')[0].scrollTop = $('#chatBox')[0].scrollHeight;
    }
  }

  nestedItems(buttons, _id, extra){
    let items = [];
    const {currentPosition} = this.state;
    if(buttons){
      buttons.forEach(function(button){
        items.push(<FlatButton key={_id+button.text} label={button.text} onClick={() => handleButtonPress(button.data,button.text, currentPosition)} />);
      });
    }
    /*
    { Promotionto: '2016-05-03T00:00:00',
I20171008-11:05:28.259(11)?         PrimarySubChannel: 'CAFE',
I20171008-11:05:28.260(11)?         PromotionFrom: '2016-02-22T00:00:00',
I20171008-11:05:28.261(11)?         Brand: 'BULMERS ORIGINAL',
I20171008-11:05:28.262(11)?         POCName: 'Eight Acres Cafe',
I20171008-11:05:28.263(11)?         PackFormat: 'BOTTLE',
I20171008-11:05:28.265(11)?         Discount: 0.06,
I20171008-11:05:28.266(11)?         Channel: 'ON PREMISE',
I20171008-11:05:28.267(11)?         Location: '-37.76770623,144.9298136',
I20171008-11:05:28.269(11)?         ABV: 4.5,
I20171008-11:05:28.269(11)?         google_map: 'https://www.google.com/maps/search/?api=1&query=-37.76770623,144.9298136',
I20171008-11:05:28.270(11)?         BrandCluster: 'CIDER & FLAVOUR',
I20171008-11:05:28.271(11)?         SKUID4: 47148,
I20171008-11:05:28.271(11)?         SizeTier: 'C' } } }
*/
    if(extra){
      items.push(<ListItem
        primaryText={extra.POCName}
        secondaryText={`Is selling ${extra.Brand} from ${moment(extra.PromotionFrom).fromNow()}`}
        rightIconButton={<FlatButton label='Directions' href={extra.google_map}></FlatButton>}
      ></ListItem>)
    }
    return items;
  }

  render() {
    const { loading, documents, match, history, user, responses} = this.props;
    console.log(responses);
    return (!loading ? (
    <Paper style={styleFooter} zDepth={1}>
    <div className="Chat">
    <Paper style={{maxHeight:'200px', overflowY:'scroll', margin:'0'}} zDepth={0}
        id='chatBox'>
    <List>
      {responses.map(({ _id, text, createdAt, type, buttons,extra}) => ( type == 'me' ?
      <div key={_id + 'd1'}>
      <ListItem
        key={_id}
        primaryText={text}
        secondaryText={moment(createdAt).fromNow()}
        leftAvatar={<Avatar>{user.profile.name.first[0]}</Avatar>}
      />
      <Divider key={_id+'d'} inset={true} />
      </div>
      :
      <div key={_id + 'd1'}>
      <ListItem
        key={_id}
        primaryText={text}
        secondaryText={moment(createdAt).fromNow()}
        rightAvatar={<Avatar src='/bestie.jpg'></Avatar>}
        initiallyOpen={true}
        nestedItems={this.nestedItems(buttons, _id,extra)}
      >
      </ListItem>
      <Divider key={_id+'d'} inset={true} />
      </div>
      ))}
    </List>
    </Paper>
    <ChatWidget history={history} location={this.state.currentPosition} />
    </div>
    </Paper>
  ) : <Loading />);
  }
}

AuthFooter.propTypes = {
  loading: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  responses: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('users.editProfile');
  const subscription2 = Meteor.subscribe('responses');
  return {
    loading: (!subscription.ready() && !subscription2.ready()),
    user: Meteor.user(),
    responses: ResponsesCollection.find({},{sort:{createdAt:1}}).fetch(),
  };
}, AuthFooter);
