import React from 'react';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import {IconMenu, IconButton, MenuItem} from 'material-ui';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

const AuthenticatedNavigation = props => (
  <IconMenu
      iconStyle={props.iconStyle}
      iconButtonElement={<IconButton><MoreVertIcon/></IconButton>}
      targetOrigin={{horizontal: 'right', vertical: 'top'}}
      anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
      <MenuItem primaryText="Profile" onClick={()=>props.history.push('/profile')} />
      <MenuItem primaryText="Sign Out" onClick={()=>props.history.push('/logout')}/>
  </IconMenu>
);

AuthenticatedNavigation.muiName = 'IconMenu';

AuthenticatedNavigation.propTypes = {
  name: PropTypes.string.isRequired,
};

export default withRouter(AuthenticatedNavigation);
