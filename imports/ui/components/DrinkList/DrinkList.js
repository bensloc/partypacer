import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';
import Loading from '../../components/Loading/Loading';
import {ListItem, List, FlatButton,IconButton, FontIcon, GridList, GridTile, Avatar, Divider, Paper, Chip} from 'material-ui';
import ResponsesCollection from '../../../api/Responses/Responses';
import DrinksCollection from '../../../api/Drinks/Drinks';
let moment = require('moment');


const style = {height: '120px'};

const imgHeight = {
  height: '100%',
  width: null
};

class DrinksList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { loading, documents, match, history, user, drinks } = this.props;
    console.log('DRINKS -------------->')
    console.log(drinks);
    return (!loading ? (
    <div className="Drinks">
    <div>
      <GridList cols={4} rows={1} cellHeight={110}>
        <GridTile cols={1}
        >
            <Chip>
                <Avatar>{user.profile.name.first[0]}</Avatar>
                {user.profile.name.first}
            </Chip>
        </GridTile>
        {drinks.map(({image, data, _id, createdAt}) =>(
            <GridTile key={_id} cols={1}
            title={data && data.Brand ? data.Brand: null}
            subtitle={data && data.Alcohol ? data.Alcohol + '% ' + 'consumed ' + moment(createdAt).fromNow()  : null}>
            <img style={imgHeight} src={`data:image/jpeg;base64, ${image}`} />
          </GridTile>
        ))}
      </GridList>
    </div>
    </div>
  ) : <Loading />);
  }
}

DrinksList.propTypes = {
  loading: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired,
  drinks: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default createContainer(({userId}) => {
  const subscription = Meteor.subscribe('users.all');
  const subscription3 = Meteor.subscribe('drinks.all');
  return {
    loading: (!subscription.ready() && !subscription3.ready()),
    user: Meteor.users.findOne({_id: userId}),
    drinks: DrinksCollection.find({owner:userId}).fetch(),
  };
}, DrinksList);
