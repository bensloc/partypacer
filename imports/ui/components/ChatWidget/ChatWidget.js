/* eslint-disable max-len, no-return-assign */

import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel, Button } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import validate from '../../../modules/validate';
import {TextField, FlatButton} from 'material-ui';
import { createContainer } from 'meteor/react-meteor-data';

class ChatWidget extends React.Component {
    constructor(props){
      super(props);
    }

    componentDidMount() {
      const component = this;

      validate(component.form, {
        rules: {
          body: {
            required: true,
          },
        },
        messages: {
          body: {
            required: 'Sorry I didn\'t get that..',
          },
        },
        submitHandler() { component.handleSubmit(); },
      });
    }
  
    handleSubmit() {
      // const { history } = this.props;
      const question = {
        text: this.body.getValue().trim(),
        location: this.props.location
      };
      console.log(question)
      Meteor.call('chat', question, (error, response) => {
        if (error) {
          Bert.alert(error.reason, 'something went wrong');
        } else {
          Bert.alert(response.reply,'success');
          this.form.reset();
        }
      });
    }

    goToCamera() {
      window.location.href = "/camera"
    }
  
    render() {
      // const { history } = this.props;
      return (
          <div>
      <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
        <TextField
            inputStyle={{textAlign:'center'}}
            className="form-control"
            name="body"
            type='text'
            ref={body => (this.body = body)}
            placeholder="Talk to me."
            autoComplete="off"
          />
        <FlatButton className='pull-right' type="submit" label='Go' />

      </form>
                  <FlatButton className='pull-right' onClick={this.goToCamera} type="submit" label='Picture' />
          </div>);
    }
  }
  
  ChatWidget.propTypes = {
    // loading: PropTypes.bool.isRequired,
    // match: PropTypes.object.isRequired,
    // history: PropTypes.object.isRequired,
    // user: PropTypes.object.isRequired,
    location: PropTypes.object,
  };
  
  // export default createContainer(() => {
  //   const subscription = Meteor.subscribe('users.editProfile');
  //   return {
  //     loading: !subscription.ready(),
  //     user: Meteor.user(),
  //   };
  // }, ChatWidget);

  export default ChatWidget;