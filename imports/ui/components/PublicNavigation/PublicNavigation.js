import React from 'react';
import {FlatButton, ToolbarGroup, Toolbar, Tabs, Tab} from 'material-ui';
import { withRouter } from 'react-router-dom';

const style = {
  color: 'white',
  margin: '6px',
}

const PublicNavigation = (props) => (
  <div>
      <FlatButton key={1} style={style} label="LOGIN" onClick={() => props.history.push('/login')}/>
      <FlatButton key={2} style={style} label="SIGN UP" onClick={() => props.history.push('/signup')}/>
  </div>
);

export default withRouter(PublicNavigation);
