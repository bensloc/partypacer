/* eslint-disable jsx-a11y/no-href*/

import React from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Roles } from 'meteor/alanning:roles';
import { Bert } from 'meteor/themeteorchef:bert';
import Navigation from '../../components/Navigation/Navigation';
import Authenticated from '../../components/Authenticated/Authenticated';
import Public from '../../components/Public/Public';
import Index from '../../pages/Index/Index';
import Documents from '../../pages/Documents/Documents';
import NewDocument from '../../pages/NewDocument/NewDocument';
import ViewDocument from '../../pages/ViewDocument/ViewDocument';
import EditDocument from '../../pages/EditDocument/EditDocument';
import Signup from '../../pages/Signup/Signup';
import Login from '../../pages/Login/Login';
import Logout from '../../pages/Logout/Logout';
import VerifyEmail from '../../pages/VerifyEmail/VerifyEmail';
import RecoverPassword from '../../pages/RecoverPassword/RecoverPassword';
import ResetPassword from '../../pages/ResetPassword/ResetPassword';
import Profile from '../../pages/Profile/Profile';
import NotFound from '../../pages/NotFound/NotFound';
import Footer from '../../components/Footer/Footer';
import AuthFooter from '../../components/AuthFooter/AuthFooter';
import Terms from '../../pages/Terms/Terms';
import Privacy from '../../pages/Privacy/Privacy';
import ExamplePage from '../../pages/ExamplePage/ExamplePage';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { Alert, Button } from 'react-bootstrap';
import {Snackbar, Paper} from 'material-ui';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {lightGreenA200} from 'material-ui/styles/colors';
import Chat from '../../pages/Chat/Chat';
import {default as CameraView} from "../../pages/Chat/CameraView"

injectTapEventPlugin();

import './App.scss';

const style = {
  padding: '2em 2em 2em 2em',
  // maxWidth: '30em',
  // margin: 'auto auto',
  height: '100%',
};

const handleResendVerificationEmail = (emailAddress) => {
  Meteor.call('users.sendVerificationEmail', (error) => {
    if (error) {
      Bert.alert(error.reason, 'danger');
    } else {
      Bert.alert(`Check ${emailAddress} for a verification link!`, 'success');
    }
  });
};

const muiTheme = getMuiTheme({
  palette: {
    backgroundColor: lightGreenA200,
  }
});

const App = props => (
  <MuiThemeProvider muiTheme={muiTheme}>
  <Router>
    {!props.loading ? <div className="App">
       {/*props.userId && !props.emailVerified ? <Snackbar message={'Your email is still unverified!'} open={true} action='Resend Verification' onActionTouchTap={() => handleResendVerificationEmail(props.emailAddress)}/> : '' }*/}
      <Navigation {...props} />
        <div className='appContent' style={{height:'100%'}}>
        <Paper style={style} zDepth={0}>
          <Switch>
            <Route exact name="index" path="/" component={Index} />
            <Authenticated exact path="/documents" component={Documents} {...props} />
            <Authenticated exact path="/documents/new" component={NewDocument} {...props} />
            <Authenticated exact path="/documents/:_id" component={ViewDocument} {...props} />
            <Authenticated exact path="/documents/:_id/edit" component={EditDocument} {...props} />
            <Authenticated exact path="/profile" component={Profile} {...props} />
            <Authenticated exact path="/chat" component={Chat} {...props} />
            <Authenticated exact path="/camera" component={CameraView} {...props} />
            <Public path="/signup" component={Signup} {...props} />
            <Public path="/login" component={Login} {...props} />
            <Route path="/logout" component={Logout} {...props} />
            <Route name="verify-email" path="/verify-email/:token" component={VerifyEmail} />
            <Route name="recover-password" path="/recover-password" component={RecoverPassword} />
            <Route name="reset-password" path="/reset-password/:token" component={ResetPassword} />
            <Route name="terms" path="/terms" component={Terms} />
            <Route name="privacy" path="/privacy" component={Privacy} />
            <Route name="examplePage" path="/example-page" component={ExamplePage} />
            <Route component={NotFound} />
          </Switch>
        </Paper>
        </div>
      { Meteor.userId() ? <AuthFooter {...props} /> : <Footer /> }
    </div> : ''}
  </Router>
  </MuiThemeProvider>
);

App.defaultProps = {
  userId: '',
  emailAddress: '',
};

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  userId: PropTypes.string,
  emailAddress: PropTypes.string,
  emailVerified: PropTypes.bool.isRequired,
};

const getUserName = name => ({
  string: name,
  object: `${name.first} ${name.last}`,
}[typeof name]);

export default createContainer(() => {
  const loggingIn = Meteor.loggingIn();
  const user = Meteor.user();
  const userId = Meteor.userId();
  const loading = !Roles.subscription.ready();
  const name = user && user.profile && user.profile.name && getUserName(user.profile.name);
  const emailAddress = user && user.emails && user.emails[0].address;

  return {
    loading,
    loggingIn,
    authenticated: !loggingIn && !!userId,
    name: name || emailAddress,
    roles: !loading && Roles.getRolesForUser(userId),
    userId,
    emailAddress,
    emailVerified: user && user.emails ? user && user.emails && user.emails[0].verified : true,
  };
}, App);
